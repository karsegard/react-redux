# React-translate
## Introduction
React & Redux utilities


## Install

  

```bash
yarn add @karsegard/react-redux

```

  

## Usage

see [example](example)
 

## License

  

MIT © [FDT2k](https://github.com/FDT2k)
