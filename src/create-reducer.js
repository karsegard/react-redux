
import { matchRule } from '@karsegard/composite-js/StringUtils'

//export const anyOf = array => ;





export const createWildcardReducer = (initialState, handlers) => (state = initialState, action) => {
    if (handlers.hasOwnProperty(action.type)) {
        return handlers[action.type](state, action)
    } else if (handlers.hasOwnProperty('default')) {
        return handlers['default'](state, action)
    } else {
        let keys = Object.keys(handlers);
        for (let key of keys) {

            let subkeys = key.split(',');

            for (let subkey of subkeys) {

                if (matchRule(subkey, action.type)) {
                    debugger;

                    return handlers[key](state, action)

                }
            }
        }
        return state
    }

}





export const createReducer = (initialState, handlers) => (state = initialState, action) => {
    if (handlers.hasOwnProperty(action.type)) {
        return handlers[action.type](state, action)
    } else if (handlers.hasOwnProperty('default')) {
        return handlers['default'](state, action)
    } else {
        return state
    }

}


export default createReducer;