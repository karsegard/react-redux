import {createAction, createAsyncAction,bindSelectors}from '@';

import { nanoid } from '@reduxjs/toolkit';
export default (getModule) => {

    const { action_types:types, selectors } = getModule()

    const actions = {};

    actions.fetch= createAction(types.FETCH)


    actions.add_item = createAction(types.ADD)

    actions.add = (item)=> (dispatch,getState)=> {

        const key = selectors.key(getState());

        if(!item[key]){
            item[key] = nanoid();
        }
       return dispatch(actions.add_item(item))
    } 

    actions.edit = createAction(types.EDIT)
    
    actions.del = createAction(types.DELETE)
    actions.set_key = createAction(types.SET_KEY)

    return actions;
}



