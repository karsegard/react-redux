import { safe_path } from '@karsegard/composite-js';
import { createSelector } from '@';




export default getModule => {
    const { baseSelector } = getModule();
    const safe_array = safe_path([]);
    const safe_object = safe_path({});

    const module = {};


    module.key = createSelector(baseSelector, state => state.key);

    module.list = createSelector(baseSelector, state => state.allIds.map(item => state.byIds[item]));
    module.byIds = createSelector(baseSelector, state =>  state.byIds);

    return module;

}
