import { combineReducers, createReducer,handlers } from '@';

import { nanoid } from '@reduxjs/toolkit';

export default (getModule) => {

    const { submodules, action_types: types,settings:{default_key} } = getModule()
    const {removeKeys,delFromList} = handlers;


    const module = {};


    const byIds = (state, {payload:{items}}) => {

        const key = state.key;
        //const ids = items.map(item => item[key]);


        let results =  items.reduce((carry, item) => {
            let pkey = item[key];
            if(!pkey){
                pkey = nanoid();
            }

            
            carry.byIds[pkey] = {...item,[key]:pkey};
            carry.allIds.push(pkey);
            return carry;
        }, {byIds:{},allIds:[]})

        return {
            ...state,
            ...results
        }
    }



    const insert = (state,{payload:item})=> {

        const key = state.key;

        let pkey = item[key];
       
        return {
            ...state,
            byIds: {
                ...state.byIds,
                [pkey]:item
            },
            allIds: [...state.allIds,pkey]
        }
    }


    const update = (state,{payload:item})=> {
        const key = state.key;
        return {
            ...state,
            byIds: {...state.byIds, [item[key]]:item}
        }
    }

    const del = (state,{payload:{id}})=> {
        const key = state.key;

        return {
            ...state,
            byIds: removeKeys(item=>{
            return     item[key]===id
            },state.byIds),
            allIds:delFromList(state.allIds,id)

        }
    }

    const key = (state,{payload}) => {

        return {
            ...state,
            key: payload
        }
    }

    module.reducer = createReducer({ key: default_key, byIds: {}, allIds: [] }, {
        [types.FETCH]: byIds,
        [types.ADD]: insert,
        [types.EDIT]: update,
        [types.DELETE]: del,
        [types.SET_KEY]: key,
        ['default'] : state => state,
    })

    return module;
}