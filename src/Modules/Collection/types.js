import { createActionTypes, createPrefixableActionTypes } from '@';

export const ACTIONS_TYPES = createActionTypes(
    'FETCH',
    'EDIT',
    'ADD',
    'DELETE',
    'SET_KEY',
   
)

export const makeActionTypes = createPrefixableActionTypes(ACTIONS_TYPES);

export default  getModule => makeActionTypes(getModule().prefix)
