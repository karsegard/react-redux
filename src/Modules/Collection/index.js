import { compose } from '@karsegard/composite-js';
import {createModule} from '@';


import makeActionTypes from './types';
import makeActions from './actions';
import makeReducer from './reducer';
import makeSelectors from './selectors';
import makeSubmodules from './submodules';


export default createModule(
    {
        submodules:makeSubmodules,
        selectors: makeSelectors,
        action_types:makeActionTypes,
        actions: makeActions,
        reducers: makeReducer,
        reducer: getModule => getModule().reducers.reducer
    },
    {
        default_key:'id'
    }
);
