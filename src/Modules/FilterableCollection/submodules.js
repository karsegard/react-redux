import { compose } from '@karsegard/composite-js';


import CollectionModule from '@/Modules/Collection';

export const createSubModules = getModule => {
    const { baseSelector, prefix,settings } = getModule();

    const module = {
        collection: CollectionModule(compose(state => state.collection, baseSelector), `${prefix}/COLLECTION`, {default_key:settings.default_key}),

    }
    return module
}


export default createSubModules;