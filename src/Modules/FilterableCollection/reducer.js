import { combineReducers, createReducer, handlers } from '@';
import {spec} from '@karsegard/composite-js'
import { nanoid } from '@reduxjs/toolkit';

export default (getModule) => {

    const { submodules, types,settings:{default_keys,mode} } = getModule()

    const module = {};


    

    module.reducer = (state = { keys:default_keys,filter: '' }, action) => {

        switch(action.type){
            case types.SET_MODE_CONTAINS:
                return {
                    ...state,
                    mode:'contain'
                }
            case types.SET_MODE_START:
                return {
                    ...state,
                    mode:'start'
                }
            case types.SET_MODE_END:
                return {
                    ...state,
                    mode:'end'
                }
            break;

            case types.SET_FILTER:
                return {
                    ...state,
                    filter:action.payload,
                }
            break;
            case types.CLEAR_FILTER:
                return {
                    ...state,
                    filter:''
                }
                break;
            default: 
                return {
                    filter: state.filter,
                    mode,
                    keys:state.keys,
                    collection: submodules.collection.reducer(state.collection,action)
                }

        }
        return state;
    }

    return module;
}