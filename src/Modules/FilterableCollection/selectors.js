import { safe_path } from '@karsegard/composite-js';
import { createSelector } from '@';




export default getModule => {
    const { baseSelector, submodules } = getModule();
    const safe_array = safe_path([]);
    const safe_object = safe_path({});

    const module = {};





    module.unfiltered_list = submodules.collection.selectors.list;
    module.collection = createSelector(baseSelector, state => state.collection);

    module.filter = createSelector(baseSelector,state=> state.filter)

    module.list = createSelector([baseSelector, module.unfiltered_list], (state, collection) => {
        const { filter, keys, mode } = state;

        if (!filter || filter === '') {
            return collection
        }

        return collection.filter(item => {


            let match = false;
            for (let key of keys) {
                if (item[key]) {
                    switch (mode) {
                        case 'start':
                            match = item[key].toUpperCase().startsWith(filter.toUpperCase());
                            break;
                        case 'end':
                            match =item[key].toUpperCase().endsWith(filter.toUpperCase());
                            break;
                        case 'contain':
                            match =item[key].toUpperCase().includes(filter.toUpperCase());
                            break;
                    }
                } else {
                    console.warn('some keys are not present in the list')
                }
            }
            return match
        })

    });


    return module;

}
