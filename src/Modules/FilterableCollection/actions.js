import {createAction, createAsyncAction,bindSelectors}from '@';


export default (getModule) => {

    const { types, selectors,submodules } = getModule()

    const actions = {
        ...submodules.collection.actions
    };

    actions.set_filter = createAction(types.SET_FILTER);
    actions.clear_filter = createAction(types.CLEAR_FILTER);

    
    return actions;
}



