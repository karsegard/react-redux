import { createActionTypes, createPrefixableActionTypes } from '@';

export const ACTIONS_TYPES = createActionTypes(
    'SET_FILTER',
    'CLEAR_FILTER',
    'SET_MODE_START',
    'SET_MODE_END',
    'SET_MODE_CONTAINS',
   
)

export const makeActionTypes = createPrefixableActionTypes(ACTIONS_TYPES);

export default  getModule => makeActionTypes(getModule().prefix)
