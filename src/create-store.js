import React from 'react';
import { combineReducers } from 'redux';
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { Provider } from 'react-redux'
import {
    persistStore,
    persistReducer,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { PersistGate } from 'redux-persist/integration/react'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import { is_type_object } from '@karsegard/composite-js';
import  devToolsEnhancer from'remote-redux-devtools';


export function createReducerManager(initialReducers) {
    if (!is_type_object(initialReducers)) {
        throw new Error("the initialReducers with registry must be an object")
    }
    // Create an object which maps keys to reducers
    const reducers = { ...initialReducers }

    // Create the initial combinedReducer
    let combinedReducer = combineReducers(reducers)

    // An array which is used to delete state keys when reducers are removed
    let keysToRemove = []

    let store ;

    return {
        getReducerMap: () => reducers,

        // The root reducer function exposed by this object
        // This will be passed to the store
        reducer: (state, action) => {
            // If any reducers have been removed, clean up their state first
            if (keysToRemove.length > 0) {
                state = { ...state }
                for (let key of keysToRemove) {
                    delete state[key]
                }
                keysToRemove = []
            }

            // Delegate to the combined reducer
            return combinedReducer(state, action)
        },

        // Adds a new reducer with the specified key
        add: (key, reducer) => {
            if (!key || reducers[key]) {
                return
            }

            // Add the reducer to the reducer mapping
            reducers[key] = reducer

            // Generate a new combined reducer
            combinedReducer = combineReducers(reducers)
            store.dispatch({type:'@manager/ADDED_REDUCER'})

        },

        addModule: (module) => {
            let key = module.prefix;
            if (!key || reducers[key]) {
                return
            }

            // Add the reducer to the reducer mapping
            reducers[key] = module.reducer
            console.log(reducers);
            // Generate a new combined reducer
            combinedReducer = combineReducers(reducers)
            store.dispatch({type:'@manager/ADDED_MODULE'})
        },

        // Removes a reducer with the specified key
        remove: key => {
            if (!key || !reducers[key]) {
                return
            }

            // Remove it from the reducer mapping
            delete reducers[key]

            // Add the key to the list of keys to clean up
            keysToRemove.push(key)

            // Generate a new combined reducer
            combinedReducer = combineReducers(reducers)
            store.dispatch({type:'@manager/REMOVED'})

        },
        bindStore: _store => {
            store = _store;
        }
    }
}



const defaultOptions = {
    persistKey: '',
    persist: false,
    manager: false,
    persistMigration: { version: 1 },
    createStoreOptions: {}
}

export const withRemoteDevTools = (options, settings = { secure: false, maxAge: 100, hostname: 'localhost', port: 8000, realtime: true }) => {
    const opts = {
        devTools: false,
        enhancers: [devToolsEnhancer(settings)]
    }

    return {
        ...options,
        createStoreOptions: {
            ...options.createStoreOptions,
            ...opts
        }
    }
}

export const createStore = (reducer, options = defaultOptions)  => {

    let initialReducer = reducer;
    let manager;
    let _Provider;

    if (options.manager===true) {
        manager = createReducerManager(reducer);
        initialReducer = manager.reducer;
    }

    if (options.persist) {
        const persistConfig = {
            key: options.persistKey,
            ...options.persistMigration,
            storage,
        }

        initialReducer = persistReducer(persistConfig, initialReducer)
    }

    const store = configureStore({
        ...options.createStoreOptions,
        reducer: initialReducer,
        middleware: [thunk, logger]
    })

    
    if (options.manager === true) {
        store.manager = manager
        manager.bindStore(store)
    }
    if (options.persist === true) {
        let persistor = persistStore(store)

        _Provider= props=>(
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    {props.children}
                </PersistGate>
            </Provider>)
    } else {
        _Provider= props=> (
            <Provider store={store}>
                {props.children}
            </Provider>)
    }

    return {Provider:_Provider,store}
}



