import {spec} from '@karsegard/composite-js/ObjectUtils'
import {merge} from '@karsegard/composite-js';


export const bindSelectors = spec;

export const combineSelectors = (...selectors) => selectors.reduce(
    (carry,selector)=>{
        return merge(carry,selector);
    },{})