
import { createModule, exportModule, suffix_key, prefix_key } from '@/module';
import { makeActionCreator } from '@/make-action'
import makeAsyncActionCreator, { makePromiseDispatcher } from '@/async-dispatch'
import { createActionTypes, createPrefixableActionTypes } from '@/types';
import { createReducer, createWildcardReducer } from '@/create-reducer'
import { makeStore, makePersistStore } from '@/make-store'
import * as handlers from '@/handlers'
import { combineReducers } from 'redux'
import { createSelector } from 'reselect';
import { bindSelectors, combineSelectors } from '@/selectors';
import { createMigrate } from 'redux-persist'

import Collection from '@/Modules/Collection';
import FilterableCollection from '@/Modules/FilterableCollection';
import { connect, useSelector, useDispatch,useStore } from 'react-redux';
import { compose } from '@karsegard/composite-js'

import {createStore,withRemoteDevTools,createReducerManager} from './create-store'

const Modules = {
    Collection,
    FilterableCollection
}
const createAction = makeActionCreator;

const createAsyncAction = makeAsyncActionCreator;

export {
    createModule,
    exportModule,
    suffix_key, prefix_key,
    makeActionCreator,
    makeAsyncActionCreator,
    makePromiseDispatcher,
    createActionTypes,
    createPrefixableActionTypes,
    createReducer,
    createWildcardReducer,
    handlers,
    makeStore,
    makePersistStore,
    bindSelectors,
    combineSelectors,

    createAction,
    createAsyncAction,

    Modules,
    //reselect
    createSelector,

    //redux
    combineReducers,


    //redux-persist
    createMigrate,


    //react-redux exports
    connect,
    useSelector,
    useDispatch,
    useStore,
    compose,

    // new store system
    createStore,withRemoteDevTools,createReducerManager


}