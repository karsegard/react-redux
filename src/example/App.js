import React, { useState, Children, cloneElement, useEffect } from 'react';
import { Store, ConnectComponentA, ConnectForm, Crud as CrudModule, FilteredCrud as FilteredCrudModule } from './redux'
import { Link,Switch, Route } from "wouter";
import { useSelector,useDispatch} from'react-redux';
import { connect, bindSelectors,createStore } from '@'
import { nanoid } from '@reduxjs/toolkit';

import { Container, LayoutFlex, LayoutFlexColumn, Fullscreen, Grid } from '@karsegard/react-core-layout';
import '@karsegard/react-core-layout/dist/style.css'

import { is_type_function, run_or_yield } from '@karsegard/composite-js'
import ExampleModule from '@/Modules/FilterableCollection';

const randomColor = _ => "#" + ((1 << 24) * Math.random() | 0).toString(16);

const Tile = props => {

    return (

        <Container cover style={{ backgroundColor: randomColor() }}>
            <LayoutFlexColumn cover justCenter alignCenter>
                {props.children}
            </LayoutFlexColumn>
        </Container>
    )
}

const Message = props => {

    return (
        <li>{props.title}</li>
    )
}


const Form = ConnectForm(props => {
    const [value, setValue] = useState('')
    const { add_message, test_wildcard, test_wildcard2 } = props;

    const handleAdd = x => add_message({ title: value }).catch(res => {
        console.log('ho no');
    })
    return (
        <>
            <input type="text" onChange={e => setValue(e.target.value)} value={value} />
            <button onClick={handleAdd}>add</button>
            <button onClick={_ => {
                test_wildcard({
                    'bloup': 'bloup'
                })
            }}>add</button>
            <button onClick={_ => {
                test_wildcard2({
                    'bloup': 'bloup'
                })
            }}>add</button>
        </>
    )
})

Form.defaultProps = {
    add_message: x => alert('no handler defined')
}

export const MessagesList = ConnectComponentA(props => {

    const { messages, add_message } = props;

    return (
        <ul>
            {messages.map((message, idx) => {

                return (<Message key={idx} {...message} />)

            })}

        </ul>
    );

})

MessagesList.defaultProps = {
    messages: []
}


const CRUDComponent = props => {

    const { fetch, list, del, edit, add, filter, set_filter } = props;
    const [edited, setEdited] = useState(-1);
    const [value, setValue] = useState('');
    useEffect(() => {
        fetch({
            items: [
                { label: 'test' },
                { label: 'test' },
                { label: 'test' },
                { label: 'test' },
                { label: 'test' },
                { label: 'test' },
                { label: 'test' },
                { label: 'test' },
                { label: 'test' },
                { label: 'test' },
            ]
        })
    }, [])


    return (<Tile>
        <h1>Crud Module</h1>

        <Container style={{ width: '500px' }}>
            <LayoutFlexColumn>
                {set_filter && <input type="text" placeholder="filter" onChange={e => set_filter(e.target.value)} value={filter} />}
                {list.map((item, idx) => {
                    return (<LayoutFlex justEvenly key={idx}>
                        {edited != idx && item.label}
                        {edited == idx && <input type="text" onChange={e => setValue(e.target.value)} value={value} />}
                        <LayoutFlex className="actions">
                            {edited != idx && <div onClick={_ => {
                                setValue(item.label)
                                setEdited(idx)

                            }}>edit</div>}
                            {edited == idx && <div onClick={_ => {
                                edit({ label: value, id: item.id })
                                setEdited(-1);
                            }}>save</div>}
                            <div onClick={_ => del({ id: item.id })}>delete</div>
                        </LayoutFlex>
                    </LayoutFlex>)
                })}
            </LayoutFlexColumn>
        </Container>
    </Tile>)

}


const CRUD = connect(
    bindSelectors(CrudModule.selectors),
    CrudModule.actions
)(CRUDComponent);

const FilteredCRUD = connect(
    bindSelectors(FilteredCrudModule.selectors),
    FilteredCrudModule.actions
)(CRUDComponent);

const ListColumn = props => {

    const { col, handleItemAction, actions, row, ...rest } = props;
    const { className, type, accessor, render } = col;

    const value = is_type_function(accessor) ? accessor(props) : row[accessor];


    const renderAction = (row)  => (key,label)=> (<div key={key} onClick={_ => handleItemAction(key, row)}>{label}</div>)

    const renderActions = _=> (<LayoutFlex justBetween className="item-actions" >{actions.map((action, act_idx) => {
        return renderAction(row)(action.key,action.label)
    })}</LayoutFlex>)

    return (
        <React.Fragment>
            {type === 'actions' && !render && <LayoutFlex justBetween className="item-actions" >
                {actions.map((action, act_idx) => {
                    return (<div key={act_idx} onClick={_ => handleItemAction(action.key, row)}>{action.label}</div>)
                })}


            </LayoutFlex>}
            {type !== "actions" && !render && <div className={className}>{value}</div>}

            {render && render(props,{renderActions,renderAction:renderAction(row)})}
        </React.Fragment>
    )
}

const ListOperation = props => {
    const { list, columns, item_actions, handleItemAction, defaultColTemplate, HeaderComponent, FooterComponent, renderHeader, renderFooter, headerHeight, footerHeight } = props
    const gridStyle = columns.map(item => item.colTemplate || defaultColTemplate).join(' ');
    return (
        <Grid templateColumns="auto" templateRows={`${headerHeight} auto ${footerHeight}`} contained cover>
            <div>
                {HeaderComponent && <HeaderComponent />}
                {renderHeader && renderHeader()}
            </div>
            <Container contained scrollable>
                <Grid templateColumns={gridStyle} columnGap="10px"  autoRows="30px">
                    {
                        list.map((row, row_idx) => {
                            return (<React.Fragment key={row_idx}>
                                {columns.map((col, col_idx) => {
                                    return (<ListColumn col={col} key={col_idx} handleItemAction={handleItemAction} actions={item_actions} row={row} />)
                                })}
                            </React.Fragment>)
                        })
                    }
                </Grid>
            </Container>
            <div>
                {FooterComponent && <FooterComponent />}
                {renderFooter && renderFooter()}
            </div>

        </Grid>
    )
}

ListOperation.defaultProps = {
    columns: [],
    defaultColTemplate: 'fit-content(30px)',
    item_actions: [{ key: 'edit', label: 'Editer' }, { key: 'del', label: 'Delete' }],
    handleItemAction: (item, action) => console.warn('handleItem not set'),
    handleAction: (item, action) => console.warn('handleAction not set'),
    headerHeight: '30px',
    footerHeight: '120px',
    list: [],
}


const StdListOperation = Component => props => {
    const [editedId, setEditedId] = useState(null);


    const handleItemAction = (action, item) => { 
        if(action == 'edit'){
            setEditedId(item.id)
        }
        else{
            console.log(`${action} on ${item.id}`)
        }

    }
    const columns = [
        { accessor: 'id', className: 'helloworld' },
        { accessor: 'name', colTemplate: 'auto' , render: ({row})=> {
            return (<>
                {row.id ===editedId && <div><input type="text" value={row.name}/></div> }
                {row.id !==editedId &&  <div>{row.name}</div> }
                </>
                )
        }},
        { accessor: 'role' ,render: ({row})=> {
            return (<>
                {row.id ===editedId && <div><select><option>admin</option><option>user</option></select></div> }
                {row.id !==editedId && <div>{row.role}</div> }
                </>
                )
        }},
        { type: 'actions', render: ({row},{renderActions,renderAction}) => {
            return (<>
                {row.id === editedId && renderAction('save','Sauver')}
                {row.id !== editedId && renderActions()}
            </>)
        }}
    ];

    return (
        <Component handleItemAction={handleItemAction} renderHeader={_=>{ return <input type="text" placeholder="filter"/>}} columns={columns} list={[
            { id: 1, name: "Fabien", role: "admin" },
            { id: 2, name: "Hector", role: "admin" },
            { id: 3, name: "Jarvis" },
            { id: 4, name: "Tony" },
            { id: 1, name: "Fabien", role: "admin" },
            { id: 2, name: "Hector", role: "admin" },
            { id: 3, name: "Jarvis" },
            { id: 4, name: "Tony" },
            
           
        ]} />
    )

}
const MyCrud = connect(
    bindSelectors(FilteredCrudModule.selectors),
    FilteredCrudModule.actions

)(StdListOperation(ListOperation))

const AppStore1 =  props => {

    return (
        <Store>
            <Fullscreen overflowY>
                <Tile>
                    <h1>Crud Editor</h1>
                    <Container style={{ width: '900px', height: '500px' }}>

                        <MyCrud />
                    </Container>
                </Tile>
                <h1>Super store</h1>
                <Form />
                <MessagesList />

                <CRUD />
                <FilteredCRUD />

            </Fullscreen>
        </Store>
    )
}

const reducers = {
    _: (state={},action)=> state
}
const {Provider,store} = createStore(reducers,{manager:true})
const FilteredCrud = ExampleModule(state => state.news, 'news',{default_keys:['label']});

store.manager.addModule(FilteredCrud);


const DumpState = props => {
    const dispatch = useDispatch();
    const state= useSelector(state => state);
    useEffect(()=>{
        dispatch(FilteredCrud.actions.fetch({items:[{id:12,label:'Hello world'},{id:13,label:'blabla'}]}))
        dispatch(FilteredCrud.actions.set_filter('hello'))
    },[])
    return (<pre>{JSON.stringify(state,null,3)}</pre>)
}
const AppStore2=  props => {
    
   

    console.log(store.manager);
    return (
        <Provider>
            <Fullscreen overflowY>
                    <DumpState/>

            </Fullscreen>
        </Provider>
    )
}

export default  props => {

    return (
        <Switch>
            <Route path="/legacy"><AppStore1/></Route>
            <Route path="/new"><AppStore2/></Route>
            <Route>
                <Link to="/legacy">Old way</Link>
                <Link to="/new">new Way</Link>
                 </Route>
        </Switch>
    )
}