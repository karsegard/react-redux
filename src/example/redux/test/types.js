import { createActionTypes, createPrefixableActionTypes } from '@';

export const ACTIONS_TYPES = createActionTypes(
   'ADD_MESSAGE'
)

export const makeActionTypes = createPrefixableActionTypes(ACTIONS_TYPES);

export default makeActionTypes
