import {makeActionCreator,makeAsyncActionCreator} from '@'

export default (getModule) => {

    const { action_types, selectors } = getModule()


    const actions = {};


    actions.add_message = makeActionCreator(action_types.ADD_MESSAGE);
    
    return actions;
}



