import { combineReducers } from 'redux';
import {createReducer} from '@';


export default (getModule) => {

    const { submodules, action_types } = getModule()


    const module = {};




    module.messagesReducer = createReducer([{title:'hello'}, {title:'world'}], {
        [action_types.ADD_MESSAGE]: (state, { payload }) => ([...state,payload])
    });


    module.reducer = combineReducers({
        messages:module.messagesReducer
    })

    return module;
}