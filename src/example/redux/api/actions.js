import {makeActionCreator,makeAsyncActionCreator} from '@'



const fakeAPICall = (args)=> new Promise((resolve,reject)=> {
    setTimeout(()=>{
        resolve(args)
    },25);
});




export default (getModule) => {

    const { action_types, selectors,submodules } = getModule()

    
    const actions = {};

    actions.setCurrentDB= makeActionCreator(action_types.SET_CURRENT_DATABASE);

    actions.add_message_success = submodules.myTodoList.actions.add_message;

    actions.add_message_fail = makeActionCreator(action_types.ADD_MESSAGE_FAIL);


    actions.add_message = args =>  makeAsyncActionCreator(actions.add_message_fail,actions.add_message_success)(fakeAPICall,args)
    
    actions.test_wildcard = makeActionCreator('TEST_BLABLOU')
    actions.test_wildcard2 = makeActionCreator('TEST2_BLABLOU')
    return actions;
}



