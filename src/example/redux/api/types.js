import { createActionTypes, createPrefixableActionTypes } from '@';

export const ACTIONS_TYPES = createActionTypes(
   'ADD_MESSAGE',
   'ADD_MESSAGE_FAIL'
)

export const makeActionTypes = createPrefixableActionTypes(ACTIONS_TYPES);

export default makeActionTypes
