import {createModule} from '@';
import { compose } from '@karsegard/composite-js';

import makeActionTypes from './types';
import makeActions from './actions';
import makeReducer from './reducer';
import makeSelectors from './selectors';

import TestModule from '../test'


const createSubModule = getModule => {
    const { baseSelector, prefix } = getModule();
    return {
        myTodoList: TestModule(compose(state => state.todoList, baseSelector), `${prefix}_TODO_LIST`),
    }

}


export default createModule(
    {
        submodules:createSubModule,
        selectors: makeSelectors,
        action_types: getModule => makeActionTypes(getModule().prefix),
        actions: makeActions,
        reducers: makeReducer,
        reducer: getModule => getModule().reducers.reducer
    }
);
