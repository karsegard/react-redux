import { combineReducers } from 'redux';
import {createReducer,createWildcardReducer} from '@';


export default (getModule) => {

    const { submodules, action_types, } = getModule()


    const module = {};


    const settings = createReducer({dbname:''},{

        [action_types.SET_CURRENT_DATABASE]: (state,action)=> {
            return {
                ...state,
                dbname: action.payload
            }
        }
    })


    const testWildcards = createWildcardReducer({},{

        ['TEST_*,TEST_BLA*']:  (state,action)=> {
            debugger;
            return {}
        },
        ['TEST2*']:  (state,action)=> {
            debugger;
            return {}
        }

    })

    module.reducer = combineReducers({
        todoList: submodules.myTodoList.reducer,
        settings: settings,
        test:testWildcards
    })

    return module;
}