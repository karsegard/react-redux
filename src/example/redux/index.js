import { combineReducers } from 'redux';
import { createMigrate } from 'redux-persist'
import { connect } from 'react-redux'
import { makeStore,bindSelectors } from '@';

import APIModuleCreator from './api';
import CrudModule from '@/Modules/Collection';
import FilteredCrudModule from '@/Modules/FilterableCollection';


export const Module = APIModuleCreator(state => state.app, '');
export const Crud = CrudModule(state => state.crud, '');
export const FilteredCrud = FilteredCrudModule(state => state.filtered, '',{default_keys:['label']});

const reducer = combineReducers({
  app: Module.reducer,
  crud: Crud.reducer,
  filtered: FilteredCrud.reducer
})


export const {
    add_message,
    test_wildcard,
    test_wildcard2
} = Module.actions;




export const {
    select_messages
} = Module.submodules.myTodoList.selectors;




const migrations = {
  0: (state) => {
    return {
      ...state,
    }
  },

}


export const ConnectComponentA  = connect(
    bindSelectors({messages:select_messages}),
    {
        add_message
    }
)

export const ConnectForm  = connect(
    null,
    {
        add_message,
        test_wildcard,
        test_wildcard2
    }
)




export const Store = makeStore('electron', reducer, { devTools: {name:'App'} }, {
  version: 1,
  migrate: createMigrate(migrations)
});
