import React from 'react'
import ReactDOM from 'react-dom'

import '@/example/index.css'
import App from '@/example/App'

ReactDOM.render(<App />, document.getElementById('root'))

